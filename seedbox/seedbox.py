import magnet
import logging
import json
import os

from filefilter import FileFilter


CONFIG_PATH = os.path.expanduser('~/.config/seed-tools')
if not os.path.isdir(CONFIG_PATH):
    os.makedirs(CONFIG_PATH)

CONFIG_FILE = os.path.join(CONFIG_PATH, 'config.json')
LOG_FILE = os.path.join(CONFIG_PATH, 'seed.log')

MAX_FAIL = 100

# Setup logging
seedlog = logging.getLogger('seed-log')
seedlog.setLevel(logging.DEBUG)

fh = logging.FileHandler(LOG_FILE)
fh.setLevel(logging.INFO)

ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)

formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
ch.setFormatter(formatter)

seedlog.addHandler(fh)
seedlog.addHandler(ch)


def logError(message):
    seedlog.error(message)
    raise Exception(message)

class SeedBox:

    def __init__(self):
        self.loadConfig()
        self.useNotification = False

    def notification(self, message):
        if not self._config.get('notify', False):
            return

        import pynotify

        if not self.useNotification:
            pynotify.init('seed-tools')
            self.useNotification = True

        pynotify.Notification('SeedTools', message).show()

    def loadConfig(self):
        if not os.path.isfile(CONFIG_FILE):
            logError('unable to open config file "%s"' % CONFIG_FILE)

        with open(CONFIG_FILE, 'r') as f:
            data = json.loads(f.read())

            self._config = data.get('config', {})
            self._hash = data.get('hash', {})
            self._files = data.get('files', {})
            self._filters = data.get('filters', {})


            self.ff = FileFilter(self._filters.get('include', []), self._filters.get('exclude', []))


        self.saveConfig()

    def getRemoteConfig(self):
        return self._config.get('remote', None)

    def saveConfig(self):
        data = {'config': self._config, 'hash':self._hash, 'files': self._files, 'filters': self._filters}

        with open(CONFIG_FILE, 'w') as f:
            json.dump(data, f, indent=4)

    def addMagnet(self, mg):
        self.getMagnetHash(mg)

        webui = self.getWebUI()
        if webui.addMagnet(mg, self._config.get('seedbox')):
            hashcode = self.getMagnetHash(mg)

            m = magnet.parseMagnet(mg)
            seedlog.info('added magnet "%s" "%s"' % (hashcode, m.get('dn', "")))

            self._hash[hashcode] = 0
            self.saveConfig()

            self.notification('Magnet Added %s' % m.get('dn', ''))

    def checkTorrentFiles(self):

        for hc in self._hash:
            seedlog.info('checking for torrentfile "%s" attempt #%s' % (hc, self._hash[hc] + 1))
            webui = self.getWebUI()

            output = webui.getFileList(hc, self._config.get('seedbox'))

            # didnt find torrent file
            if output is None:
                self._hash[hc] += 1

                if self._hash[hc] > MAX_FAIL:
                    seedlog.error('removing torrent file "%s" as failure count has been reached (%d)' % (hc, MAX_FAIL))
                    del self._hash[hc]
                self.saveConfig()
                continue

            self.loadConfig()

            toadd = []
            for f in output:
                if self.ff.shouldInclude(f):
                    toadd.append(f)
                    seedlog.info('watching file "%s"' % f)
                else:
                    logging.info('skipping "%s" as file was excluded' % f)

            self._files[hc] = toadd

            del self._hash[hc]

            self.saveConfig()

    def checkDownloadComplete(self):
        for hc in self._files:
            files = self._files[hc]
            for f in files:

                webui = self.getWebUI()
                uri = webui.checkDownloadComplete(f, self._config.get('seedbox'))
                if uri is False:
                    break

                seedlog.info('file "%s" has finished downloading adding to aria2 (%s)' % (f, uri))

                download = self.getDownloader()
                if download.addURI(uri, self._config.get('download')):
                    self.loadConfig()
                    self._files[hc].remove(f)
                    if len(self._files[hc]) == 0:
                        del self._files[hc]
                    self.saveConfig()
                    self.notification('download started %s' % f)

    def getWebUI(self):
        ui = self._config.get('seedbox').get('ui')
        if (ui == 'whatbox'):
            import ui.whatbox as whatbox
            return whatbox
        else :
            raise Error('Unable to find WebUI %s' % ui)

    def getDownloader(self):
        download = self._config.get('download').get('ui')
        if download == 'aria2':
            import ui.aria2 as aria2
            return aria2
        else:
            logError('Unable to find download UI %s' % download)


    def getMagnetHash(self, mg):
        '''
            @param mg {String} Magnet link
            @return {String} Hex encoded string of the magnets hash (in upper case).
        '''
        m = magnet.parseMagnet(mg)
        if m is False:
            logError('unable to parse "%s" as a magnet link' % mg)

        xt = m.get('xt', None)

        if xt is None:
            logError('unable to find "xt" option in magnet link "%s"' % mg)

        xt = xt.split(':')
        hashcode = xt.pop()

        if len(hashcode) == 40:
            # standard hex hash
            return hashcode.upper()

        if len(hashcode) == 32:
            # base32 encoded hash
            import base64
            return base64.b16encode(base64.b32decode(hashcode)).upper()

        logError('unable to decode hashcode from magnet "%s"' % mg)
