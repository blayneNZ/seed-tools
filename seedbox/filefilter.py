import re

class FileFilter:

    def __init__(self, include, exclude):
        self.include = []
        self.exclude = []

        for x in include:
            self.include.append(re.compile(x))

        for x in exclude:
            self.exclude.append(re.compile(x))

    '''

    @returns {Boolean} Wheter the filename should be included.
    '''
    def shouldInclude(self, filename):
        fsearch = filename.lower()
        if self.__checkInclude(fsearch):
            if self.__checkExclude(fsearch):
                # explicitily excluded
                return False

            return True

        # not implicitly included
        return False


    '''
    @returns {Boolean} Wheter the file should be included.
    '''
    def __checkInclude(self, filename):
        if len(self.include) == 0:
            return True

        for inc in self.include:
            if self.match(inc, filename):
                return True

        return False

    '''

    @returns {Boolean} Wheter the file should be excluded.
    '''
    def __checkExclude(self, filename):
        if len(self.exclude) == 0:
            return False

        for exc in self.exclude:
            if self.match(exc, filename):
                return True

        return False


    def match(self, pattern, filename):

        if pattern.match(filename) is None:
            return False

        return True
