

def parseMagnet(mg):
    if not mg.startswith('magnet:?'):
        # not a magnet file
        return False

    opts = {}
    curopt = ''
    curoptvalue = ''
    currentOption = False
    for i in xrange(8, len(mg)):
        m = mg[i]
        if not currentOption:
            if m == '=':
                currentOption = True
                curoptvalue  = ''
            else:
                curopt += m

            continue

        if m == '&':
            currentOption = False
            if opts.get(curopt, None) is None:
                opts[curopt] = curoptvalue
            else:
                oldopt = opts.get(curopt)
                if isinstance(oldopt, basestring):
                    opts[curopt] = [curoptvalue]
                else:
                    oldopt.append(curoptvalue)

            curopt = ''

            continue

        curoptvalue += m

    return opts
