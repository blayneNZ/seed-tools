import requests
import urllib
import logging
import os

seedlog = logging.getLogger('seed-log')

URL = '%(scheme)s://%(host)s%(path)s/php/addtorrent.php?url=%(magnet)s'
TORRENT = '%(scheme)s://%(host)s/private/files/%(hashcode)s.meta'
FILE = '%(scheme)s://%(host)s/private/completed/%(filename)s'
LOGIN = '%(scheme)s://%(host)s%(login)s'
COOKIES = {'Xlogin': 'XocW%2BbA7jljP06coMPgFXf1M0yJDixtaapF69tvh%2FvXDfAnTo5mZSgQOVWqmy2z0exmJM3G5sqRKtkJYyS0sS%2BGH3nOEco5T35ouF6NK%2FzJ18o0YmHogay%2BVHoFoABe%2B'}

def getScheme(config):
    if config.get('ssl', False) is False:
        return 'http'
    else :
        return'https'

def login(config):
    seedlog.info('attempting to login to host %s@%s' % (config.get('username'), config.get('host')) )
    url = LOGIN % {'host': config.get('host'), 'scheme': getScheme(config), 'login': '/login'}

    r = requests.post(url, {'username': config.get('username'), 'password':config.get('password') })

    xlogin = r.cookies.get('Xlogin', None)
    if xlogin is None:
        seedlog.warn('unable to login to server "%s"' % url)
        return False

    COOKIES['Xlogin'] = xlogin
    return True

def addMagnet(magnet, config):

    url = URL % { 'scheme': getScheme(config), 'host': config.get('host'), 'path': config.get('path'), 'magnet': urllib.quote(magnet) }

    if COOKIES.get('Xlogin', None) is None:
        if not login(config):
            return False

    r = requests.post(url, cookies=COOKIES)

    if r.status_code != 200:
        # try seedlog in again
        if not login(config):
            return False

        # resubmit the post
        r = requests.post(url, cookies=COOKIES)

        if r.status_code != 200:
            seedlog.error('unable to post magnet "%s"' % magnet)
            return

    # did it add?
    if r.text.find('addTorrentFailedURL') > 0:
        seedlog.error('unable to add magnet "%s" [%s]' % (magnet, r.text))
        return False

    return True


def getFileList(hashcode, config):
    from requests.auth import HTTPBasicAuth

    url = TORRENT % { 'scheme': getScheme(config), 'host': config.get('host'), 'path': config.get('path'), 'hashcode': hashcode }

    r = requests.get(url, auth=HTTPBasicAuth(config.get('username'), config.get('password')))
    if r.status_code != 200:
        print 'status_code %d' % r.status_code
        return None

    import bencode
    try:
        torrent = bencode.bdecode(r.content)

        output = []
        name = torrent.get('name')
        offset = 0
        if 'files' in torrent:
            for f in torrent.get('files'):
                file_path = os.sep.join([name] + f['path'])
                output.append(file_path)
        else:
            output.append( name)

        return output

    except Exception, e:
        print e
        return None


def checkDownloadComplete(filename, config):
    from requests.auth import HTTPBasicAuth
    url = FILE % { 'scheme': getScheme(config), 'host': config.get('host'), 'path': config.get('path'), 'filename': filename }

    r = requests.head(url, auth=HTTPBasicAuth(config.get('username'), config.get('password')))

    if r.status_code != 200:
        return False

    return url
