import logging
import json
import requests
import xmlrpclib

seedlog = logging.getLogger('seed-log')
URI = '%(scheme)s://%(host)s:%(port)d/rpc'


def getScheme(config):
    if config.get('ssl', False) is False:
        return 'http'
    else :
        return 'https'

def addURI(uri, config):
    url = URI % {'scheme': getScheme(config), 'host': config.get('host'), 'port': config.get('port')}
    s = xmlrpclib.ServerProxy(url)

    result = s.aria2.addUri([uri],{})

    seedlog.info('download added %s' % result)

    return True
